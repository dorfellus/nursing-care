angular.module('nursingApp').component('filters', {
    templateUrl: 'components/filters.component.html',
    bindings: {
        items: '<',
        onItemsFiltered: '&'
    },
    controllerAs: 'filtersCtrl',
    controller: ['$scope', '$rootScope', '$filter',
        function ($scope, $rootScope, $filter) {
            var vm = this;
            $scope.typeSelected = "All";
            $scope.languageSelected = "All";
            $scope.ageSlider = {
                minValue: 0,
                maxValue: 120,
                options: {
                    floor: 0,
                    ceil: 120,
                }
            };
            $scope.filteredItems = [];

            $scope.RefreshFilters = function () {
                let filteredItems = [];
                let item;
                //Filter items
                for (let index in $scope.filtersCtrl.items) {
                    //Filter Type
                    item = $scope.filtersCtrl.items[index];
                    let isPatient = item instanceof Patient;
                    let isPatientChosen = ($scope.typeSelected == "Patients");
                    if ((isPatient != isPatientChosen) && ($scope.typeSelected != "All")) {
                        continue;
                    }
                    //Filter Language
                    if (($scope.languageSelected != "All") && (item.Language != $scope.languageSelected)) {
                        continue;
                    }
                    //Filter Age
                    let minAge = $scope.ageSlider.minValue;
                    let maxAge = $scope.ageSlider.maxValue;
                    if ((minAge > 0) || (maxAge < 120)) {
                        let age = $filter('ageFilter')(item.Birthday);
                        if (age == null || age < minAge || age > maxAge) {
                            continue;
                        }
                    }
                    filteredItems.push(item);
                }
                $scope.filteredItems = filteredItems;
                $scope.filtersCtrl.onItemsFiltered({filteredItems: filteredItems});
                return $scope.filteredItems;
            };

            $scope.$on('slideEnded', function () {
                $scope.$apply(function () {
                    $scope.RefreshFilters();
                });
            });

            $scope.ClearFiltersClicked = function () {
                $scope.typeSelected = "All";
                $scope.languageSelected = "All";
                $scope.ageSlider = {
                    minValue: 0,
                    maxValue: 120,
                    options: {
                        floor: 0,
                        ceil: 120,
                    }
                };
                $scope.RefreshFilters();
            }

            $scope.SearchName = function () {
                $scope.RefreshFilters();
                let currPerson;
                for (let index in $scope.filteredItems) {
                    currPerson = $scope.filteredItems[index];
                    let currFullName = currPerson.First + ' ' + currPerson.Last;
                    //Search person
                    if (currFullName == $scope.PersonChosen.First + ' ' + $scope.PersonChosen.Last) {
                        $rootScope.$emit('PersonChosen', $scope.PersonChosen);
                    }
                }
            }
        }]
});
