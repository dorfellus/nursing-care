angular.module('nursingApp').component('mainMap', {
    templateUrl: 'components/map.component.html',
    bindings: {
        items: '<'
    },
    controllerAs: 'mapCtrl',
    controller: ['$scope', '$rootScope', 'NgMap', function ($scope, $rootScope, NgMap) {
        var vm = this;
        $scope.defaultLat = 32.0853;
        $scope.defaultLon = 34.7818;
        NgMap.getMap().then(function (map) {
            $scope.map = map;
            $scope.map.markers = [];
            refreshItems();
            // Create the DIV to hold the control and call the CenterControl()
            // constructor passing in this DIV.
            var centerControlDiv = document.createElement('div');
            var centerControl = new CenterControl(centerControlDiv, $scope.map);

            centerControlDiv.index = 1;
            $scope.map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
        })

        vm.$onChanges = function (changesObj) {
            if (changesObj.items.currentValue) {
                refreshItems();
            }
        };

        function refreshItems() {
            if ($scope.map == null || $scope.mapCtrl.items == null) {
                return;
            }

            setMapOnAll(null);
            if($scope.markerCluster){
                $scope.markerCluster.clearMarkers();
            }

            for (var i = 0; i < $scope.mapCtrl.items.length; ++i) {
                let item = $scope.mapCtrl.items[i];
                //Skips on people with no Lat and Lon data
                if (item.Lat == "") {
                    continue;
                }
                let icon = item instanceof Patient ?
                    {url: 'img/RedDot.png', scaledSize: new google.maps.Size(20, 20)} :
                    {url: 'img/BlueDot.png', scaledSize: new google.maps.Size(15, 15)};
                let marker = new google.maps.Marker({
                    id: $scope.map.markers.length.toString(),
                    position: {lat: Number(item.Lat), lng: Number(item.Lon)},
                    map: $scope.map,
                    icon: icon,
                    title: item.First + ' ' + item.Last,
                    data: item
                });
                marker.addListener('click', function () {
                    $scope.openDetails(marker.data, marker.id)
                });
                $scope.map.markers.push(marker);
            }

            if ($scope.clusterChecked) {
                $scope.markerCluster = new MarkerClusterer($scope.map, $scope.map.markers,
                    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
            }
        }

        $scope.openDetails = function (person, index) {
            $scope.currentPerson = person;
            $scope.currentMarker = $scope.map.markers[index];
            $scope.map.showInfoWindow('personDetails', index.toString());
        };

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < $scope.map.markers.length; i++) {
                $scope.map.markers[i].setMap(map);
            }
            $scope.map.markers = [];
        }

        $scope.$watch("clusterChecked", function () {
            if ($scope.clusterChecked) {
                $scope.markerCluster = new MarkerClusterer($scope.map, $scope.map.markers,
                    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
            } else if ($scope.markerCluster != null) {
                $scope.markerCluster.clearMarkers();
                refreshItems();
            }
        })

        $rootScope.$on("PersonChosen", function (e, selectedPerson) {
            for (let i = 0; i < $scope.map.markers.length; ++i) {
                if ($scope.map.markers[i].data.First === selectedPerson.First &&
                    $scope.map.markers[i].data.Last === selectedPerson.Last) {
                    $scope.map.panTo($scope.map.markers[i].position);
                    $scope.map.setZoom(17);

                    $scope.openDetails(selectedPerson, i);
                    return;
                }
            }
        });

        /**
         * The CenterControl adds a control to the map that recenters the map on
         * Chicago.
         * This constructor takes the control DIV as an argument.
         * @constructor
         */
        function CenterControl(controlDiv, map) {
            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to recenter the map';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control interior.
            var controlText = document.createElement('div');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px';
            controlText.innerHTML = 'Center Map';
            controlUI.appendChild(controlText);

            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', function () {
                if ($scope.currentMarker) {
                    $scope.map.hideInfoWindow('personDetails', $scope.currentMarker);
                }
                $scope.map.setZoom(12);
                $scope.map.panTo(new google.maps.LatLng($scope.defaultLat, $scope.defaultLon));
            });
        }
    }]
});
