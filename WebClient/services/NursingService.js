nursingApp = angular.module('nursingApp');
nursingApp.factory('nursingService',['$http', function($http){
    function readFile(path){
        return $http.get(path)
            .then(function(response){
                /*/\\r\\n|\\n/*/
                var allText = response.data.split(/[\r\n]+/);
                var headers = allText[0].split(',');
                /*var lines = [];*/
                var person = {};
                var persons = [];
                var patientsFile = false;

                //Checks if the file contains caregivers/patients.
                if (path.toString().toLowerCase().includes("patients")){
                    patientsFile = true;
                }

                //Reads the File
                for ( var i = 1; i < allText.length; i++) {
                    // split content based on comma
                    var data = allText[i].split(',');
                    if (data.length == headers.length) {
                        var cells = [];
                        for ( var j = 0; j < headers.length; j++) {
                            cells.push(data[j]);
                        }
                        if (cells[1]!=="") {
                            /*lines.push(tarr);*/
                            if(patientsFile){
                                person = new Patient(cells[1], cells[0], cells[4], cells[3], cells[5], cells[6],
                                    cells[2], cells[7], cells[8], cells[9], cells[10], cells[11]);
                            }else {
                                person = new Caregiver(cells[1], cells[0], cells[2], cells[3], cells[4], cells[5], cells[6], cells[7]
                                    ,cells[8], cells[9], cells[10], cells[11], cells[12], cells[13], cells[14], cells[15],
                                    cells[16]);
                            }
                            persons.push(person);
                        }
                    }
                }
                return persons;
            })
    }

    return {
        readFile: readFile
    }
}]);
