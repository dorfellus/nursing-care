function Caregiver(First, Last, Area, City, Phone1, Phone2, Address, Status, ID, Review, Passport, Gender, CareLevel,
                Language, Birthday, Lat, Lon) {
    Person.call(this, First, Last, Area, City, Phone1, Phone2, Address, Gender, CareLevel,
        Language, Lat, Lon);
    this.Status = Status;
    this.ID = ID;
    this.Review = Review;
    this.Passport = Passport;
    this.Birthday = Birthday;
}
