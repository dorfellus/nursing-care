function Person(First, Last, Area, City, Phone1, Phone2, Address, Gender, CareLevel,
                Language, Lat, Lon){
    this.First = First;
    this.Last = Last;
    this.Area = Area;
    this.City = City;
    this.Phone1 = Phone1;
    this.Phone2 = Phone2;
    this.Address = Address;
    this.Gender = Gender;
    this.CareLevel = CareLevel;
    this.Language = Language;
    this.Lat = Lat;
    this.Lon = Lon;
}
