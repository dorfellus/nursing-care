function Patient(First, Last, Area, City, Phone1, Phone2, Address, Gender, CareLevel,
                 Language, Lat, Lon) {
    Person.call(this, First, Last, Area, City, Phone1, Phone2, Address, Gender, CareLevel,
        Language, Lat, Lon);

    this.isPatient = true;
}
