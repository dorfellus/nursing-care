angular.module('nursingApp', ['ngMap', 'rzModule', 'ui.bootstrap', 'ngAnimate', 'ngSanitize']);

angular.module('nursingApp').controller('nursingController', ['$scope', 'nursingService', '$q',
    function ($scope, nursingService, $q) {
        $scope.Persons = [];
        $scope.FilteredPersons = [];
        let promise1 = nursingService.readFile('data/caregivers-parsed.csv');
        let promise2 = nursingService.readFile('data/patients-parsed.csv');
        $q.all([promise1, promise2]).then(function (result) {
            $scope.Persons = result[0].concat(result[1]);
            $scope.FilteredPersons = angular.copy($scope.Persons);
        });

        $scope.ItemsFiltered = function (filteredItems) {
            $scope.FilteredPersons = filteredItems;
        }
    }]);
