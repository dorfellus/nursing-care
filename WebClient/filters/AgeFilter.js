var nursingApp = angular.module('nursingApp');
nursingApp.filter('ageFilter', function() {
    function calculateAge(birthday) { // birthday is a date
        var birthday = moment(birthday, "DD/MM/YYYY");
        if (birthday.isValid()){
            return moment().diff(birthday, 'years');
        }
    }

    return function(birthdate) {
        return calculateAge(birthdate);
    };
});
