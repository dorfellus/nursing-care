Nursing Care Location System
============================
This project shows the location of patients and caregivers from a given database on a google map so that the user will be able to create matches.
The menu bar allows filtering patients and caregivers by age, language and type as well as searching for a specific patients/caregiver to focus on on the map. 
The project contains a side script that matches coordinates to each of the patient's/caregiver's address in order to present them on the map, using Nominatim API.

Demo:
https://nursing-care-1530043870004.firebaseapp.com/

Tech Stack:
1. AngularJS
2. Bootstrap 4
3. Google Maps API
4. Nominatim API
5. C#