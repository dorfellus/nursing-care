﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using RestSharp;
using System.Text.RegularExpressions;
using System.Threading;
using Newtonsoft.Json;

namespace GetCoordinates
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathCaregivers = "data/caregivers.csv";
            string pathPatients = "data/patients.csv";
            string pathCaregiversNew = "caregivers_parsed.csv";
            string pathPatientsNew = "patients_parsed.csv";
            File.Copy(pathCaregivers, pathCaregiversNew, true);
            File.Copy(pathPatients, pathPatientsNew, true);

            addCoordinates(pathCaregiversNew, false);
            addCoordinates(pathPatientsNew, true);
        }

        public static void addCoordinates(string path, bool isPatient) {
            List<string> lines = File.ReadAllLines(path).ToList();
            string[] currLine;
            string address;
            Coordinates coords;

            //add new column to the header row
            lines[0] += ",lat,lon";
            int index = 1;

            //add new column value for each row.
            lines.Skip(1).ToList().ForEach(line =>
            {
                //read address
                currLine = lines[index].Split(',');

                string coordsString = ",,";
                string street = "";
                string town = "";
                if (isPatient)
                {
                    street = currLine[2];
                    town = currLine[3];
                }
                else
                {
                    street = currLine[6];
                    town = currLine[3];
                }

                if (street != "")
                {
                    address = street + ", " + town + ", ישראל";
                    //get Coordinates from nominatim
                    coords = GetCoordinates(address);
                    if ((coords.lat != 0) && (coords.lon != 0))
                    {
                        coordsString = "," + coords.lat + "," + coords.lon;
                    }
                }
                //add lat and lon to lines
                lines[index] += coordsString;
                Console.WriteLine(lines[index]);
                index++;
            });

            //write the new content to the csv
            File.WriteAllLines(path, lines);
        }

        public static Coordinates GetCoordinates(string address)
        {
            Thread.Sleep(1200);
            //Get response from server
            var client = new RestClient("https://nominatim.openstreetmap.org/search?format=json&q=" + address);
            var request = new RestRequest(Method.GET);
            IRestResponse jsonResponse = client.Execute(request);

            //Extract lat and lon
            Double lat = 0;
            Double lon = 0;
            if (jsonResponse.IsSuccessful == false)
            {
                Console.WriteLine("Limit Exceeded");
            }
            else if ((jsonResponse.Content != "[]") && (jsonResponse.IsSuccessful == true))
            {
                dynamic deserializedResponse = JsonConvert.DeserializeObject(jsonResponse.Content);
                lat = deserializedResponse[0].lat;
                lon = deserializedResponse[0].lon;
            }
            else
            {
                Console.WriteLine("Address not found: " + address);
            }

            //return coordinates
            Coordinates coords = new Coordinates(lat, lon);
            return coords;
        }
    }
}
