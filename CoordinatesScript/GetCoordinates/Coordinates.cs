﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetCoordinates
{
    public class Coordinates
    {
        public Double lat;
        public Double lon;

        public Coordinates(Double x, Double y)
        {
            lat = x;
            lon = y;
        }
    }

}
